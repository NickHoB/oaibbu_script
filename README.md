# oai_scenario

This project is based on M-CORD `cord-4.1` branch.  
And only support `cord-4.1` branch.  

`cord-5.0` support is work in progress, will support in the furture.

## Installation

```
# only for oai epc
git clone https://github.com/aweimeow/oai_scenario ~/oai_scenario
cd ~/oai_scenario

# include oai vbbu
git clone https://gitlab.com/NickHoB/oaibbu_script.git ~/oaibbu_script
cd ~/oaibbu_script

```

Use `start.sh` to overwrite original files.  
This script will delete original M-CORD services directory,  
and use modified services source code instead.

```bash
bash start.sh
```

And execute following commands to build OAI M-CORD.

```
cd ~/cord/build
make PODCONFIG=mcord-oai-virtual.yml config
make -j4 build
```

### Create OAI BBU instance from CORD GUI

- Click Oaibbuservice
- OAI BBU service instances
- Add
- Choose owner id OAIBBU_Service
- Save
- After few minutes, you can check at instances page

![](https://i.imgur.com/yc4Kcbe.png)


![](https://i.imgur.com/MaXFX1m.png)

### Command for using OAI BBU

```shell=
ssh head1
scp ~/.ssh/id_rsa ubuntu@10.1.0.14:~/.ssh
ssh ubuntu@10.1.0.14
ssh oai@<management IP>

sudo su
cd openairinterface/cmake_targets/lte_build_oai/build/

# for execute develop branch at RCC side
sudo ./lte-softmodem -E -O ../../../targets/PROJECTS/GENERIC-LTE-EPC/CONF/rcc.band7.tm1.50PRB.nfapi.conf
```

### Check service instance

```shell=

ssh head1

vagrant@head1:~$ source /opt/cord_profile/admin-openrc.sh
vagrant@head1:~$ nova list --all-tenants
+--------------------------------------+-------------------+--------+------------+-------------+-----------------------------------------------------------------+
| ID                                   | Name              | Status | Task State | Power State | Networks                                                        |
+--------------------------------------+-------------------+--------+------------+-------------+-----------------------------------------------------------------+
| 0b2aebaf-f5de-421e-8d09-bf27619d33bc | mysite_oaibbu1-5  | ACTIVE | -          | Running     | management=172.27.0.6; public=10.8.1.4; oaibbu_network=10.0.4.2 |
| 64ece792-9e7f-4125-8fd5-9e5cb90234cd | mysite_oaispgw1-1 | ACTIVE | -          | Running     | management=172.27.0.2; public=10.8.1.2; vspgw_network=10.0.8.2  |
| 958ad833-205e-489a-99a8-cf637e813366 | mysite_oaispgw1-4 | ACTIVE | -          | Running     | management=172.27.0.4; public=10.8.1.3; vspgw_network=10.0.8.3  |
| 9d2f47b6-e4e3-4f77-812b-878749185b1a | mysite_vhss1-3    | ACTIVE | -          | Running     | management=172.27.0.5; vhss_network=10.0.7.2                    |
| ed42a403-25c7-4dfd-a3c4-f188997b85ba | mysite_vmme1-2    | ACTIVE | -          | Running     | management=172.27.0.3; vmme_network=10.0.6.2                    |
+--------------------------------------+-------------------+--------+------------+-------------+-----------------------------------------------------------------+
```


### ONOS flow

You can check `flow` and `group` information in ONOS WebUI.

```
ssh -NfL 0.0.0.0:8182:0.0.0.0:8182 head1
```

and access `http://<your_CORD_ip>:8182/onos/ui` to check flows.

## Usage

```
ssh head1
head1$ scp ~/.ssh/id_rsa ubuntu@10.1.0.14:~/.ssh
head1$ ssh ubuntu@10.1.0.14

# SSH into Service Instance, reference to nova output
ubuntu@multicolored-jump:~$ ssh ubuntu@172.27.0.2

# This is vHSS Service Instance
ubuntu@vhss:~$ sudo su -
root@vhss:~$ cd ~/openair-cn/SCRIPT/
root@vhss:~/openair-cn/SCRIPT$ ./run_hss

# This is vMME Service Instance
ubuntu@nano:~$ sudo su -
root@nano:~$ cd ~/openair-cn/SCRIPT/
root@nano:~/openair-cn/SCRIPT$ ./run_mme

# This is vSPGW Service Instance
ubuntu@spgw:~$ sudo su -
root@spgw:~$ cd ~/openair-cn/SCRIPT/
root@spgw:~/openair-cn/SCRIPT$ ./run_spgw
```

As following snapshot:  
![](https://raw.githubusercontent.com/aweimeow/oai_scenario/master/snapshot.png)

## Reserve field for automation

IP Automation:

```bash
ssh <hostname>@<IP> 'bash -s' < ip_setting.sh
```

Conf Automation:


## LICENSE

Source Code in this project is under MIT License.  
Open Air Interface image is built by [Open Air Interface Source
Code](https://gitlab.eurecom.fr/oai/openair-cn).  
It is under Apache License v2 (APLv2).
